"""
Traditional retrieval 
Algorithm using co_occurance
Author Adam Mustapha
"""

import json

class Co_occurance:

    def __init__(self, file, jum, interval):

        self.jump = jum
        self.raw_query = raw_input('Query: ')
        self.occurance = []
        self.interval = interval
        self.collection = json.load(open(file, 'r'))
        self.doc_ids = []
        self.user_query = self.token_nize(self.raw_query)
        self.relevant_colls ={}
        
        
    
    def token_nize(self, sentence):
        stp_w = json.load(open("stp_words.json", 'r'))
        sentence = sentence.lower().split(" ")
        return [sabo for sabo in sentence if sabo not in stp_w]
        
    def slice_sen(self, sentence, st, end):
        return sentence[st:end]

    def get_relevant_docs(self):

        for col in self.collection:
            sentence = self.token_nize(self.collection[col]['content'])
            start = 0
            end = self.jump
            sliced = self.slice_sen(sentence, start, end)
            while len(sliced)==self.jump:
                sliced = self.slice_sen(sentence, start, end)
                start +=1
                end +=1
                st2 =0
                end2=self.jump
                query = self.slice_sen(self.user_query, st2, end2)
                while len(query)==self.jump:
                    query = self.slice_sen(self.user_query, st2, end2)
                    if query==sliced:
                        #update relevant collections
                        new_col ={
                            col:{
                                "id":self.collection[col]['id'],
                                "content":self.collection[col]['content'],
                            },
                        }
                        self.relevant_colls.update(new_col)
                    st2 +=1
                    end2 +=1
        #search through the relevant collection
        for col in self.relevant_colls:
            sentence = self.token_nize(self.relevant_colls[col]['content'])
            st = 0
            end = self.jump
            sliced = self.slice_sen(sentence, st, end)
            occurance =0
            while len(sliced)==self.jump:
                sliced = self.slice_sen(sentence, st, end)
                st +=1
                end +=1
                st2 =0
                end2=self.jump
                query = self.slice_sen(self.user_query, st2, end2)
                while len(query)==self.jump:
                    query = self.slice_sen(self.user_query, st2, end2)
                    if query==sliced:
                        occurance+=1   
                    st2+=1
                    end2+=1
            self.occurance.append(occurance)
            self.doc_ids.append(self.relevant_colls[col]['id'])
        return self.relevant_colls
        
    def get_position(self, scores):
        no_of_rel_doc = range(1, self.interval+1)
        filtered_doc = []
        for value in scores:
            ind = scores.index(value)
            position =1
            for score in scores:
                if value>=score:
                    pass
                else:
                    position+=1
            if position in no_of_rel_doc:
                filtered_doc.append(ind)
        return filtered_doc


    def get_documents(self):
        final_ids =[]
        for do in self.get_position(self.occurance):
            final_ids.append(self.doc_ids[do])
        if len(self.get_position(self.occurance))>0:
            for col in self.relevant_colls:
                if self.relevant_colls[col]['id'] in final_ids:
                    print 'Document{}: {}'.format(self.relevant_colls[col]['id'], self.relevant_colls[col]['content'])
        else:
            print 'No relevant document found'

while True:
    try:
     c_occ= Co_occurance("collection.json", 3, 1)
     docs = c_occ.get_relevant_docs()
     c_occ.get_documents()
    
    except(KeyboardInterrupt, EOFError, SystemExit):
        break