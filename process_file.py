from bs4 import BeautifulSoup
import json


class DocToJson:
    def __init__(self, docs):

        mycollection = {}

        for doc in docs:

            with open(doc, 'r') as file:

                content = BeautifulSoup(file, features="xml")
                begin = content.find('BEGIN')

                for doc in begin.find_all('DOC'):


                    doc_id = doc.find("DOCID").text.rstrip()
                    headline = doc.find("HL").text.rstrip()
                    desc= doc.find("LP").text.rstrip()
                    text = doc.find("TEXT").text.rstrip()


                    collections ={

                        'doc{}'.format(doc_id):{

                            "id":doc.find("DOCID").text.rstrip(),
                            "title":doc.find("HL").text.rstrip(),
                            "desc":doc.find("LP").text.rstrip(),
                            "content":doc.find("TEXT").text.rstrip(),

                        },
                    }

                    mycollection.update(collections)

            with open ('out.json', 'w') as file:
                file.write(json.dumps(mycollection))
        
doc2json = DocToJson(["docs sample.txt"])